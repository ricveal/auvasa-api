const express = require("express");
const app = express();
const service = require("./service");

app.get("/stop/:id", (req, res) => {
  const codigoParada = req.params.id;
  const line = req.query.line;
  const prm = req.query.prm === "" || req.query.prm === "true";
  service.getStopTimetable(codigoParada).then(data => {
    let filteredTimetable = data.timetable;
    if (line) {
      filteredTimetable = filteredTimetable.filter(bus => bus.line === line);
    }
    if (prm || req.query.prm === "false") {
      filteredTimetable = filteredTimetable.filter(bus => bus.prm === prm);
    }
    res.json({
      id: data.id,
      busStopName: data.busStopName,
      timetable: filteredTimetable
    });
  });
});

module.exports = app;
