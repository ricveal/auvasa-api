const request = require("request-promise");
const cheerio = require("cheerio");
const headers = require("./header");

const host = "http://www.auvasa.es";

const getStopTimetable = async stopId => {
  const namespace = "parada.asp";
  const url = `${host}/${namespace}?codigo=${stopId}`;

  const html = await request({
    uri: url,
    headers,
    gzip: true,
    encoding: "binary"
  });
  const $ = cheerio.load(html, { decodeEntities: false });

  const busStopName = $(".col_three_fifth > h5")
    .text()
    .replace(/\s+/g, " ")
    .trim();

  const timetable = $(".table-hover tbody > tr")
    .map((i, element) => {
      const $td = $(element).find("td");

      const line = $td.eq(0).text();
      const prm = Boolean($td.eq(1).children().length);
      const destination = $td.eq(3).text();
      const minRemaining = Number($td.eq(4).text());

      return { line, prm, destination, minRemaining };
    })
    .toArray();

  const data = { id: stopId, busStopName, timetable };
  return data;
};

module.exports = {
  getStopTimetable
};
