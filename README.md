# Auvasa API

API REST no oficial que expone un servicio para la consulta del estado de una parada de la red de bus público de Valladolid
para su consulta e integración en posibles aplicaciones de terceros.

El servicio utiliza un "scrapper" para parsear la web y devuelve la información de tiempos que es posible obtener a través
de la web http://www.auvasa.es/parada.asp

Además, se permite el uso de filtros para buscar por una línea específica o limitar la búsqueda a aquellos buses que cuenten con rampa para personas con movilidad reducida.

## Instalación

Este proyecto está desarrollado bajo tecnología Node.js por lo que su instalación se realiza mediante `npm` o herramientas semejantes como `yarn`.

Clone o descargue este repositorio en su equipo y a continuación, ejecute `npm i` o `yarn` según su herramienta de preferencia.

## Uso

Una vez instalada la aplicación, es posible arrancarla mediante el comando `yarn start` o `npm run start` que levantará el servicio REST en el puerto 5555 de tu ordenador, haciendo posible el acceso mediante [http://localhost:5555/stop/813](http://localhost:5555/stop/813). En este ejemplo, buscaremos la información en tiempo real para la parada nº 813 que corresponde con Plaza España Bola del Mundo.

Como ejemplo adiciones:

- [http://localhost:5555/stop/813?line=7](http://localhost:5555/stop/813?line=7): nos devolverá la información filtrada para la linea 7 exclusivamente.
- [http://localhost:5555/stop/813?prm](http://localhost:5555/stop/813?prm): nos devolverá la información filtrada para aquellos autobuses que cuenten con rampa para personas con movilidad reducida.
- [http://localhost:5555/stop/813?line=7&prm](http://localhost:5555/stop/813?line=7&prm): es posible combinar ambos filtros.

## Arquitectura

El proyecto emplea las siguientes librerías:

- [cheerio](https://github.com/cheeriojs/cheerio): parseo de HTML en servidor empleando una API similar a la de JQuery.
- [request-promise](https://github.com/request/request-promise): librería para llevar a cabo peticiones HTTP de forma simplificada y con soporte para promesas.
- [express](https://expressjs.com/): framework web para Node.js con el que construimos la API REST

## Agradecimientos

Este proyecto se basa en el trabajo anterior de @luisddm en [Auvasa Scrapper](https://github.com/luisddm/auvasa-scraper)
